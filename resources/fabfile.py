import os

from fabric.api import *
from datetime import datetime


LIVE = {
    'name': 'eia',
    'host': 'eia@eia.smouter.org',
    'vhost_dir': '/home/eia',
    'dumps_dir': '/home/eia/dumps',
    'vcs_dir': '/home/eia/app/vcs',
    'virtualenv': '/home/eia/app/pyenv',
    'dbhost': 'localhost',
    'dbuser': 'eia',
    'dbname': 'eia',
}


def live():
    env.vars = LIVE
    env.hosts = [LIVE['host']]
    env.use_ssh_config = True


def _manage(command):
    with prefix('source %s/bin/activate' % env.vars['virtualenv']):
        run('python %s/src/manage.py %s' % (env.vars['vcs_dir'], command))


def update():
    with cd(env.vars['vcs_dir']):
        run('git pull')


def update_full():
    update()
    requirements()
    migrate()
    static()
    restart()


def update_static():
    update()
    static()
    restart()


def static():
    _manage('collectstatic --noinput -i src -i templates -i scss')
    _manage('create_error_pages')


def static_clear():
    _manage('collectstatic --noinput -c -i src -i templates -i scss')
    _manage('create_error_pages')


def migrate():
    dump()
    _manage('migrate')


def restart():
    run('sudo supervisorctl restart %s' % env.vars['name'])


def dump():
    with cd(env.vars['dumps_dir']):
        run('pg_dump -h %s -U %s -d %s > `date --rfc-3339=seconds | sed "s/ /_/g"`.sql' % (
            env.vars['dbhost'], env.vars['dbuser'], env.vars['dbname']))


def cleanup_pyc():
    with cd(env.vars['vcs_dir']):
        run('find . -name "*.py[co]" -delete')


def requirements():
    with prefix('source %s/bin/activate' % env.vars['virtualenv']):
        run('pip install -r %s' % os.path.join(
            env.vars['vcs_dir'], 'resources', 'requirements-server.txt'))


def dump_media():
    name = datetime.now().strftime('%Y%m%d_%H_%M_%S')
    with cd(os.path.join(env.vars['vhost_dir'], 'web')):
        path = os.path.join('/tmp', 'media_%s.tar.gz' % name)
        run('tar -czf %s media' % path)
        run('du -h %s' % path)
        get(path, os.path.join(os.path.dirname(__file__), '..'))
        run('rm %s' % path)


def dump_data():
    name = datetime.now().strftime('%Y%m%d_%H_%M_%S')
    path = os.path.join('/tmp', 'dump_%s.sql' % name)
    run('pg_dump -h %s -U %s -d %s > %s' % (
        env.vars['dbhost'], env.vars['dbuser'], env.vars['dbname'], path))
    get(path, os.path.join(os.path.dirname(__file__), '..'))
    run('rm %s' % path)
