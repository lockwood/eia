import json

import pytest
from django.core.cache import cache
from django.core.urlresolvers import reverse

from eia.core.utils import timestamp_from_datetime
from testing.factories.uploads import UploadFactory, EntryFactory


@pytest.mark.django_db
class TestUploadListView:

    def setup(self):
        cache.clear()
        self.cache_key = 'uploadlistview_GET'

    def teardown(self):
        cache.clear()

    def test_method_not_allowed(self, client):
        response = client.post(reverse('api:uploads'))
        assert response.status_code == 405
        json_response = json.loads(response.content.decode('utf-8'))
        assert json_response['message'] == 'Method not allowed'

    def test_get_success_empty(self, client):
        assert not cache.get(self.cache_key)

        response = client.get(reverse('api:uploads'))
        assert response.status_code == 200
        json_response = json.loads(response.content.decode('utf-8'))
        assert not json_response['uploads']

        assert cache.get(self.cache_key).content == response.content

    def test_get_success_list(self, client):
        assert not cache.get(self.cache_key)

        upload1 = UploadFactory.create()
        upload2 = UploadFactory.create()

        response = client.get(reverse('api:uploads'))
        assert response.status_code == 200
        json_response = json.loads(response.content.decode('utf-8'))
        uploads = json_response['uploads']
        assert len(json_response['uploads']) == 2

        assert uploads[0] == {
            'title': upload1.title,
            'slug': upload1.slug,
            'timestamp': timestamp_from_datetime(upload1.timestamp),
            'detail': reverse('api:upload', kwargs={'slug': upload1.slug}),
        }

        assert uploads[1] == {
            'title': upload2.title,
            'slug': upload2.slug,
            'timestamp': timestamp_from_datetime(upload2.timestamp),
            'detail': reverse('api:upload', kwargs={'slug': upload2.slug}),
        }

        assert cache.get(self.cache_key).content == response.content


@pytest.mark.django_db
class TestUploadDetailView:

    def setup(self):
        cache.clear()

        self.upload = UploadFactory.create()
        self.entry1 = EntryFactory.create(upload=self.upload)
        self.entry2 = EntryFactory.create(upload=self.upload)

    def teardown(self):
        cache.clear()

    def test_method_not_allowed(self, client):
        response = client.post(reverse('api:upload', kwargs={'slug': self.upload.slug}))
        assert response.status_code == 405
        json_response = json.loads(response.content.decode('utf-8'))
        assert json_response['message'] == 'Method not allowed'

    def test_get_404(self, client):
        response = client.get(reverse('api:upload', kwargs={'slug': 'doesnotexist'}))
        assert response.status_code == 404
        json_response = json.loads(response.content.decode('utf-8'))
        assert json_response['message'] == 'No upload found'

    def test_get_success(self, client):
        cache_key = 'uploaddetailview_GET_{0}'.format(self.upload.slug)
        assert not cache.get(cache_key)

        response = client.get(reverse('api:upload', kwargs={'slug': self.upload.slug}))
        assert response.status_code == 200
        json_response = json.loads(response.content.decode('utf-8'))
        assert json_response == {
            'title': self.upload.title,
            'slug': self.upload.slug,
            'timestamp': timestamp_from_datetime(self.upload.timestamp),
            'entries': [
                {
                    'title': self.entry2.title,
                    'description': self.entry2.description,
                    'image': self.entry2.image.url
                },
                {
                    'title': self.entry1.title,
                    'description': self.entry1.description,
                    'image': self.entry1.image.url
                },
            ]
        }

        assert cache.get(cache_key).content == response.content
