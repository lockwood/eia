import os

import mock
import pytest
from django.conf import settings
from PIL import Image
from requests import RequestException
from requests.models import Response

from eia.uploads.forms import UploadForm
from eia.uploads.models import Upload, Entry
from testing.factories.uploads import UploadFactory, EntryFactory
from testing.utils import open_file


@pytest.mark.django_db
class TestUploadForm:

    def test_form_invalid(self):
        form = UploadForm({})
        assert not form.is_valid()
        assert len(form.errors) == 3
        assert 'csv' in form.errors
        assert 'title' in form.errors
        assert 'slug' in form.errors

    @mock.patch('requests.get')
    @mock.patch('requests.models.Response.content')
    def test_form_valid(self, content_mock, response_mock):
        content_mock.__get__ = mock.PropertyMock(
            return_value=Image.open(os.path.join(settings.RESOURCES_DIR, 'image.jpg')).tobytes())

        ResponseMock = Response()
        ResponseMock.status_code = 200
        ResponseMock.headers['content-type'] = 'image/jpg'
        response_mock.return_value = ResponseMock

        form = UploadForm(
            {
                'title': 'Title 1',
                'slug': 'title-1',
            },
            {
                'csv': open_file('test.csv'),
            }
        )
        assert form.is_valid()
        assert not form.errors

        upload = form.save()
        assert Upload.objects.count() == 1
        assert upload.timestamp

        assert Entry.objects.count() == 3
        entries = Entry.objects.all()

        entry1 = entries[0]
        assert entry1.title == 'Item 1'
        assert entry1.description == 'Description 1'
        assert entry1.image

        entry2 = entries[1]
        assert entry2.title == 'Item 2'
        assert entry2.description == 'Description 2'
        assert not entry2.image

        entry3 = entries[2]
        assert entry3.title == 'This is a longer description that sho...'
        assert entry3.description == 'Description 3'
        assert not entry3.image

        assert form.soft_errors == [
            'Row: 3 Column: 3 > No image found.',
            'Row: 4 Column: 1 > Title too long (40 characters).',
            'Row: 4 Column: 3 > No image found.'
        ]

    def test_form_csv_empty(self):
        form = UploadForm(
            {
                'title': 'Title 1',
                'slug': 'title-1',
            },
            {
                'csv': open_file('empty.csv'),
            }
        )
        assert not form.is_valid()
        assert len(form.errors) == 1
        assert form.errors['csv'] == [
            'Row: 2 Column: 1 > Title is missing.',
            'Row: 3 Column: 1 > Title is missing.'
        ]

        assert form.soft_errors == [
            'Row: 2 Column: 3 > No image found.',
            'Row: 3 Column: 3 > No image found.'
        ]

    @mock.patch('requests.get')
    @mock.patch('requests.models.Response.content')
    def test_form_csv_image_url_404(self, content_mock, response_mock):
        content_mock.__get__ = mock.PropertyMock(return_value='')

        ResponseMock = Response()
        ResponseMock.status_code = 404
        response_mock.return_value = ResponseMock

        form = UploadForm(
            {
                'title': 'Title 1',
                'slug': 'title-1',
            },
            {
                'csv': open_file('short.csv'),
            }
        )
        assert form.is_valid()
        assert not form.errors

        upload = form.save()
        assert Upload.objects.count() == 1
        assert upload.timestamp

        assert Entry.objects.count() == 1
        entries = Entry.objects.all()

        entry1 = entries[0]
        assert entry1.title == 'Item 1'
        assert entry1.description == 'Description 1'
        assert not entry1.image

    @mock.patch('requests.get')
    @mock.patch('requests.models.Response.content')
    def test_form_csv_image_url_requests_exception(self, content_mock, response_mock):
        content_mock.__get__ = mock.PropertyMock(return_value='')
        response_mock.side_effect = RequestException()

        form = UploadForm(
            {
                'title': 'Title 1',
                'slug': 'title-1',
            },
            {
                'csv': open_file('short.csv'),
            }
        )

        assert form.is_valid()
        assert not form.errors

        upload = form.save()
        assert Upload.objects.count() == 1
        assert upload.timestamp

        assert Entry.objects.count() == 1
        entries = Entry.objects.all()

        entry1 = entries[0]
        assert entry1.title == 'Item 1'
        assert entry1.description == 'Description 1'
        assert not entry1.image

    @mock.patch('requests.get')
    @mock.patch('requests.models.Response.content')
    def test_form_update(self, content_mock, response_mock):
        # Create existing instance
        upload = UploadFactory.create()
        entry = EntryFactory.create(upload=upload)
        assert Upload.objects.count() == 1
        assert Entry.objects.count() == 1

        content_mock.__get__ = mock.PropertyMock(
            return_value=Image.open(os.path.join(settings.RESOURCES_DIR, 'image.jpg')).tobytes())

        ResponseMock = Response()
        ResponseMock.status_code = 200
        ResponseMock.headers['content-type'] = 'image/jpg'
        response_mock.return_value = ResponseMock

        form = UploadForm(
            {
                'title': upload.title,
                'slug': upload.slug,
            },
            {
                'csv': open_file('test.csv'),
            },
            instance=upload
        )
        assert form.is_valid()
        assert not form.errors

        upload = form.save()
        assert Upload.objects.count() == 1
        assert upload.timestamp

        # check that original entry was removed
        assert not Entry.objects.filter(pk=entry.pk)

        assert Entry.objects.count() == 3
        entries = Entry.objects.all()

        entry1 = entries[0]
        assert entry1.title == 'Item 1'
        assert entry1.description == 'Description 1'
        assert entry1.image

        entry2 = entries[1]
        assert entry2.title == 'Item 2'
        assert entry2.description == 'Description 2'
        assert not entry2.image

        entry3 = entries[2]
        assert entry3.title == 'This is a longer description that sho...'
        assert entry3.description == 'Description 3'
        assert not entry3.image
