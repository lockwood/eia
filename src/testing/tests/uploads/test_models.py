import pytest
from django.core.cache import cache

from testing.factories.uploads import UploadFactory, EntryFactory


@pytest.mark.django_db
class TestUploadModel:

    def setup(self):
        cache.clear()
        self.upload = UploadFactory.create()

    def teardown(self):
        cache.clear()

    def test_str(self):
        assert str(self.upload) == self.upload.slug

    def test_serialize(self):
        serialized = self.upload.serialize()
        assert len(serialized) == 3
        assert serialized['title'] == self.upload.title
        assert serialized['slug'] == self.upload.slug
        assert serialized['timestamp'] == self.upload.timestamp

    def test_delete_cache_on_create_1(self):
        cache_key = 'uploadlistview_GET'
        cache.set(cache_key, 'test', 3600)
        assert cache.get(cache_key)

        UploadFactory.create()
        assert not cache.get(cache_key)

    def test_delete_cache_on_create_2(self):
        cache_key = 'uploaddetailview_GET_{0}'.format(self.upload.slug)
        cache.set(cache_key, 'test', 3600)
        assert cache.get(cache_key)

        self.upload.save()
        assert not cache.get(cache_key)

    def test_delete_cache_on_delete_1(self):
        cache_key = 'uploadlistview_GET'
        cache.set(cache_key, 'test', 3600)
        assert cache.get(cache_key)

        self.upload.delete()
        assert not cache.get(cache_key)

    def test_delete_cache_on_delete_2(self):
        cache_key = 'uploaddetailview_GET_{0}'.format(self.upload.slug)
        cache.set(cache_key, 'test', 3600)
        assert cache.get(cache_key)

        self.upload.delete()
        assert not cache.get(cache_key)


@pytest.mark.django_db
class TestEntryModel:

    def setup(self):
        self.entry = EntryFactory()

    def test_str(self):
        expected = '{0} - {1}'.format(self.entry.pk, self.entry.upload_id)
        assert expected == str(self.entry)

    def test_serialize(self):
        serialized = self.entry.serialize()
        assert len(serialized) == 3
        assert serialized['title'] == self.entry.title
        assert serialized['description'] == self.entry.description
        assert serialized['image'] == self.entry.image
