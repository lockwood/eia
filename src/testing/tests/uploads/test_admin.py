import os

import mock
import pytest
from django.conf import settings
from django.core.urlresolvers import reverse
from PIL import Image
from requests.models import Response

from eia.uploads.models import Upload
from testing.utils import open_file, get_messages_from_cookie


@pytest.mark.django_db
class TestUploadAdmin:

    @mock.patch('requests.get')
    @mock.patch('requests.models.Response.content')
    def test_post_success_messages(self, content_mock, response_mock, admin_client):
        content_mock.__get__ = mock.PropertyMock(
            return_value=Image.open(os.path.join(settings.RESOURCES_DIR, 'image.jpg')).tobytes())

        ResponseMock = Response()
        ResponseMock.status_code = 200
        ResponseMock.headers['content-type'] = 'image/jpg'
        response_mock.return_value = ResponseMock

        response = admin_client.post(reverse('admin:uploads_upload_add'), {
            'title': 'Title 1',
            'slug': 'title-1',
            'csv': open_file('test.csv'),
        }, follow=False)

        assert response.status_code == 302

        messages = get_messages_from_cookie(response.cookies)
        assert len(messages) == 4

        msg1 = list(messages)[0]
        assert msg1.tags == 'warning'
        assert msg1.message == 'Row: 3 Column: 3 > No image found.'

        msg2 = list(messages)[1]
        assert msg2.tags == 'warning'
        assert msg2.message == 'Row: 4 Column: 1 > Title too long (40 characters).'

        msg3 = list(messages)[2]
        assert msg3.tags == 'warning'
        assert msg3.message == 'Row: 4 Column: 3 > No image found.'

        msg4 = list(messages)[3]
        assert msg4.tags == 'success'

        assert Upload.objects.count() == 1
