import factory

from eia.uploads.models import Upload, Entry


class UploadFactory(factory.DjangoModelFactory):

    title = factory.Sequence(lambda i: 'Title {0}'.format(i))
    slug = factory.Sequence(lambda i: 'slug-{0}'.format(i))
    csv = factory.django.FileField(from_path='')

    class Meta:
        model = Upload


class EntryFactory(factory.DjangoModelFactory):

    upload = factory.SubFactory(UploadFactory)
    title = factory.Sequence(lambda i: 'Title {0}'.format(i))
    description = factory.Sequence(lambda i: 'Description {0}'.format(i))
    image = factory.django.ImageField(filename='test.png', height=10, width=10)

    class Meta:
        model = Entry
