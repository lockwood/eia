import os

from django.conf import settings
from django.contrib.messages.storage.cookie import CookieStorage
from django.core.files.uploadedfile import SimpleUploadedFile
from django.http import HttpRequest


def open_file(filename):
    with open(os.path.join(settings.RESOURCES_DIR, filename)) as f:
        return SimpleUploadedFile(f.name, bytes(f.read(), 'utf-8'))


def get_messages_from_cookie(cookies):
    request = HttpRequest()
    request.COOKIES = {CookieStorage.cookie_name: cookies.get(
        CookieStorage.cookie_name).value}
    return CookieStorage(request)
