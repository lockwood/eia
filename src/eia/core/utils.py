import calendar
from datetime import datetime

import pytz
from django.utils import timezone


def datetime_from_timestamp(ts):
    return datetime.utcfromtimestamp(ts).replace(tzinfo=pytz.utc)


def timestamp_from_datetime(dt):
    # convert to utc
    dt_utc = dt.astimezone(timezone.utc)
    return calendar.timegm(dt_utc.timetuple())
