import os

from eia.conf.global_settings import *


VHOST_DIR = '/home/eia'

SECRET_KEY = 'live'
ALLOWED_HOSTS = ['eia']
RAVEN_JS_WHITELIST_URLS = ['eia']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'eia_live',
        'USER': 'eia',
        'HOST': 'localhost',
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': 'localhost:11211',
        'KEY_PREFIX': 'eia_live',
    }
}

STATIC_ROOT = os.path.normpath(os.path.join(VHOST_DIR, 'web', 'static'))
MEDIA_ROOT = os.path.normpath(os.path.join(VHOST_DIR, 'web', 'media'))

LOGGING['handlers'].update({
    'file': {
        'class': 'logging.FileHandler',
        'filename': os.path.normpath(os.path.join(VHOST_DIR, 'log', 'django.log')),
    },
})
LOGGING['loggers'] = {
    'root': {
        'level': 'WARNING',
        'handlers': ['file', 'sentry'],
    },
}
