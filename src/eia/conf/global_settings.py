import os
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS


PROJECT_DIR = os.path.dirname(os.path.dirname(__file__))
ROOT_DIR = os.path.dirname(os.path.dirname(PROJECT_DIR))

ROOT_URLCONF = 'eia.urls'
WSGI_APPLICATION = 'eia.wsgi.application'

SECRET_KEY = 'Y U NO SECRET KEY Y!?'
ALLOWED_HOSTS = []
RAVEN_JS_WHITELIST_URLS = []

DEBUG = TEMPLATE_DEBUG = False

DEFAULT_FROM_EMAIL = SERVER_EMAIL = 'eia@example.com'
EMAIL_SUBJECT_PREFIX = os.path.basename(PROJECT_DIR)

LANGUAGE_CODE = 'en'
TIME_ZONE = 'Europe/Amsterdam'
USE_I18N = True
USE_L10N = True
USE_TZ = True

USE_ETAGS = True

SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'raven.contrib.django.raven_compat',

    'django_babel',
    'statici18n',

    'eia.accounts',
    'eia.core',
    'eia.uploads',
)

AUTH_USER_MODEL = 'accounts.User'

LOCALE_PATHS = (
    os.path.join(PROJECT_DIR, 'accounts/locale'),
    os.path.join(PROJECT_DIR, 'core/locale'),

    os.path.join(ROOT_DIR, 'static/locale'),
    os.path.join(ROOT_DIR, 'templates/locale'),
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'eia.api.middleware.ApiExceptionMiddleware',
)

MEDIA_ROOT = os.path.join(ROOT_DIR, 'web', 'media')
MEDIA_URL = '/media/'

STATIC_ROOT = os.path.join(ROOT_DIR, 'web', 'static')
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(ROOT_DIR, 'static'),
)
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)
STATICI18N_ROOT = os.path.join(ROOT_DIR, 'static', 'js')
STATICI18N_OUTPUT_DIR = 'i18n'

TEMPLATE_DIRS = (
    os.path.join(ROOT_DIR, 'templates'),
)
TEMPLATE_CONTEXT_PROCESSORS += (
    'django.core.context_processors.request',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'django.utils.log.NullHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
    },
    'formatters': {
        'verbose': {
            'format':
                '[%(asctime)s] %(levelname)s:%(name)s %(funcName)s %(message)s',  # noqa
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'loggers': {
        # This is the root logger that catches everything, if there's no other
        # match on the logger name. If we want custom logging handing for our
        # code vs. third-party code, define loggers for each module/app
        # that's using standard python logging.
        'root': {
            'level': 'DEBUG',
            'handlers': ['console'],
        },

        'eia': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'django': {
            'level': 'INFO',
            'handlers': ['console'],
            'propagate': False,
        },
    }
}


RAVEN_JS_DSN = ''
