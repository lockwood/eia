from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin


urlpatterns = []


if settings.DEBUG:
    urlpatterns += [
        url(
            r'^%s(?P<path>.*)$' % settings.MEDIA_URL.strip('/'),
            'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}
        ),
    ]


urlpatterns += [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include('eia.api.urls', namespace='api')),
]
