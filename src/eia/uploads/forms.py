import csv
import logging
import os
import uuid

import requests
from django import forms
from django.core.files.base import ContentFile
from django.template.defaultfilters import truncatechars, striptags

from .models import Upload, Entry


logger = logging.getLogger(__name__)


class UploadForm(forms.ModelForm):

    csv_error = 'Row: {0} Column: {1} > {2}'

    def __init__(self, *args, **kwargs):
        self.soft_errors = []
        super(UploadForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Upload
        fields = ['title', 'slug', 'csv']

    def clean(self):
        cleaned_data = super(UploadForm, self).clean()
        csv_file = cleaned_data.get('csv', None)
        if csv_file:
            content = csv_file.read()
            reader = csv.reader(content.decode('utf-8').splitlines())

            parsed = []
            for row_index, row in enumerate(reader):
                if row_index == 0:
                    continue

                parsed.append({
                    'title': self._clean_title(row[0], row_index + 1, 1),
                    'description': self._clean_description(row[1], row_index + 1, 2),
                    'image': self._clean_image(row[2], row_index + 1, 3),
                })

            cleaned_data['entries'] = parsed
        return cleaned_data

    def _clean_title(self, data, r, c):
        if not data:
            self.add_error('csv', self.csv_error.format(r, c, 'Title is missing.'))
        if len(data) > 40:
            self.soft_errors.append(self.csv_error.format(r, c, 'Title too long (40 characters).'))
            return truncatechars(data, 40)
        return data

    def _clean_description(self, data, r, c):
        return striptags(data)

    def _clean_image(self, url, r, c):
        image = None
        if url:
            try:
                response = requests.get(url)
                logger.debug('Fetched: {0}'.format(url))
            except requests.RequestException as e:
                logger.debug(e)
                return image

            if response.ok:
                if response.headers['content-type'].startswith('image/'):
                    logger.debug('Found image for url: {0}'.format(url))
                    name = uuid.uuid4().hex
                    original = os.path.basename(url).split('.')
                    if len(original) > 1:
                        name = '{0}.{1}'.format(name, original[-1])
                    return ContentFile(response.content, name=name)

            logger.debug('No image found for url: {0}'.format(url))

        if not image:
            self.soft_errors.append(self.csv_error.format(r, c, 'No image found.'))

        return image

    def create_entries(self, instance):
        bulk = []
        for entry in self.cleaned_data['entries']:
            bulk.append(
                Entry(
                    upload_id=instance.pk,
                    title=entry['title'],
                    description=entry['description'],
                    image=entry['image'],
                )
            )
        Entry.objects.bulk_create(bulk)

    def save(self, commit=True):
        existing = bool(self.instance.pk)

        instance = super(UploadForm, self).save(commit=False)
        instance.save()

        if not existing:
            self.create_entries(instance)
        else:
            # Delete and create new entries
            Entry.objects.filter(upload_id=instance.pk).delete()
            self.create_entries(instance)

        return instance
