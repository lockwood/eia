from django.contrib import admin
from django.contrib import messages
from django.utils.translation import gettext_lazy as _

from .forms import UploadForm
from .models import Upload, Entry


class UploadAdmin(admin.ModelAdmin):

    list_display = ('title', 'slug', 'timestamp', 'get_entries')
    prepopulated_fields = {
        'slug': ('title',)
    }
    form = UploadForm

    def get_entries(self, obj):
        return obj.entries.count()
    get_entries.short_description = _('Entries')

    def save_model(self, request, obj, form, change):
        soft_errors = getattr(form, 'soft_errors', [])
        [messages.add_message(request, messages.WARNING, msg) for msg in soft_errors]
        super(UploadAdmin, self).save_model(request, obj, form, change)


class EntryAdmin(admin.ModelAdmin):

    list_display = ('title', 'upload', 'image')


admin.site.register(Upload, UploadAdmin)
admin.site.register(Entry, EntryAdmin)
