from django.core.cache import cache
from django.db import models
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.forms.models import model_to_dict
from django.utils import timezone
from django.utils.translation import gettext_lazy as _


class Upload(models.Model):

    title = models.CharField(_('Title'), max_length=100)
    slug = models.SlugField(_('Slug'), unique=True)
    csv = models.FileField(_('CSV'))
    timestamp = models.DateTimeField(_('Timestamp'), default=timezone.now)

    class Meta:
        verbose_name = _('Upload')
        verbose_name_plural = _('Uploads')
        ordering = ['timestamp']

    def __str__(self):
        return '{0}'.format(self.slug)

    def serialize(self):
        return model_to_dict(
            self, fields=[
                'title', 'slug', 'timestamp'
            ]
        )


class Entry(models.Model):

    upload = models.ForeignKey(Upload, verbose_name=_('Upload'), related_name='entries')
    title = models.CharField(_('Title'), max_length=40)
    description = models.TextField(_('Description'), blank=True)
    image = models.ImageField(_('Image'), blank=True, null=True)
    optimized = models.ImageField(_('Optimized'), blank=True, null=True)

    class Meta:
        verbose_name = _('Entry')
        verbose_name_plural = _('Entries')

    def __str__(self):
        return '{0} - {1}'.format(self.pk, self.upload_id)

    def serialize(self):
        return model_to_dict(
            self, fields=[
                'title', 'description', 'image'
            ]
        )


def clear_cache(instance):
    cache.delete('uploadlistview_GET')
    cache.delete('uploaddetailview_GET_{0}'.format(instance.slug))


@receiver(post_save, sender=Upload)
def clear_cache_upload_save(sender, instance=None, created=False, **kwargs):
    clear_cache(instance)


@receiver(post_delete, sender=Upload)
def clear_cache_upload_delete(sender, instance=None, **kwargs):
    clear_cache(instance)
