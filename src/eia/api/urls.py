from django.conf.urls import patterns, url

from .views import UploadListView, UploadDetailView


urlpatterns = patterns('',
    url(r'uploads/$', UploadListView.as_view(), name='uploads'),
    url(r'uploads/(?P<slug>[-\w]+)/$', UploadDetailView.as_view(), name='upload'),
)
