import datetime
import json

from django.db.models.fields.files import ImageFieldFile
from django.db.models.query import QuerySet as DjangoQuerySet

from eia.core.utils import timestamp_from_datetime


class CustomJsonEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return timestamp_from_datetime(obj)

        if isinstance(obj, ImageFieldFile):
            if obj:
                return obj.url
            return None

        if isinstance(obj, DjangoQuerySet):
            return [o.serialize() for o in obj]

        return json.JSONEncoder.default(self, obj)
