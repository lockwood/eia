from django.core.cache import cache
from django.core.urlresolvers import reverse
from django.http import JsonResponse
from django.utils.translation import ugettext as _
from django.views.generic import View

from .exceptions import ApiError
from .serializers import CustomJsonEncoder
from eia.uploads.models import Upload


class ApiView(View):

    cache_kwargs = []

    def __init__(self, **kwargs):
        self.context = {}
        super(ApiView, self).__init__(**kwargs)

    def http_method_not_allowed(self, request, *args, **kwargs):
        raise ApiError(405, _('Method not allowed'))

    def get(self, request, *args, **kwargs):
        return JsonResponse(self.context, encoder=CustomJsonEncoder)

    @property
    def cache_key(self):
        key = '{0}_{1}'.format(self.__class__.__name__.lower(), self.request.method)
        if self.cache_kwargs:
            key = key + '_' + '_'.join([self.kwargs[arg] for arg in self.cache_kwargs])
        return key


def cache_page(time=3600):

    ''' Cache decorator for the ApiView class'''

    def decorator(fn):
        def wrapper(self, request, *args, **kwargs):
            cache_key = self.cache_key

            result = cache.get(cache_key)
            if not result:
                result = fn(self, request, *args, **kwargs)
                cache.set(cache_key, result, time)

            return result
        return wrapper

    return decorator


class UploadListView(ApiView):

    http_method_names = [u'get', ]

    @cache_page()
    def dispatch(self, request, *args, **kwargs):
        uploads = Upload.objects.all()
        self.context['uploads'] = []
        for upload in uploads:
            self.context['uploads'].append({
                'title': upload.title,
                'slug': upload.slug,
                'timestamp': upload.timestamp,
                'detail': reverse('api:upload', kwargs={'slug': upload.slug})
            })
        return super(UploadListView, self).dispatch(request, *args, **kwargs)


class UploadDetailView(ApiView):

    http_method_names = [u'get', ]
    cache_kwargs = ['slug']

    @cache_page()
    def dispatch(self, request, *args, **kwargs):
        slug = self.kwargs['slug']
        try:
            upload = Upload.objects.get(slug=slug)
        except Upload.DoesNotExist:
            raise ApiError(404, _('No upload found'))

        self.context.update({
            'title': upload.title,
            'slug': upload.slug,
            'timestamp': upload.timestamp,
            'entries': upload.entries.all()
        })
        return super(UploadDetailView, self).dispatch(request, *args, **kwargs)
