from django.http import JsonResponse

from .exceptions import ApiError
from .serializers import CustomJsonEncoder


class ApiExceptionMiddleware(object):

    def process_exception(self, request, exception):
        if isinstance(exception, ApiError):
            return self.json_response(exception.status_code, exception.message)
        return None

    def json_response(self, status, message):
        return JsonResponse({'message': message}, encoder=CustomJsonEncoder, status=status)
