eia
==============================================================================

Installation
------------

.. note::

    Please make sure npm is set to use your global python. ``gyp`` does not
    support running on python3 :-(
    ``npm config set python /usr/bin/python2``


.. code-block:: bash

    $ Create your virtualenv (you should have installed virtualenvwrapper)
    $ mkvirtualenv eia -p python3.4

    $ # Clone repository
    $ git clone git@bitbucket.org:lockwood/eia.git

    $ # Activate Environment and install
    $ source env/bin/activate
    $ make devinstall

    $ # run tests
    $ make tests


Development settings
--------------------

Create a new file named ``settings.py`` in the ``src/eia`` folder with the
following content.

.. code-block:: python

    from eia.conf.dev_settings import *

And adapt the settings to your environment.


Setup the database
------------------

.. code-block:: bash

    $ createdb database_name
    $ python src/manage.py migrate


Staring the server & superuser
------------------------------

.. code-block:: bash

    $ # Create a new super user
    $ python src/manage.py createsuperuser

Now you can run the webserver and start using the site.

.. code-block:: bash

   $ python src/manage.py runserver

This starts a local webserver on `localhost:8000 <http://localhost:8000/>`_. To
view the administration interface visit `/admin/ <http://localhost:8000/admin/>`_

Grunt tasks
-----------

- Run ``grunt validate`` to validate the source files
- Run ``grunt test`` to run all javascript tests
- Run ``grunt build`` to build all the sources for a deploy

Resources
---------

* `Documentation <yu no url>`_
* `Code <https://bitbucket.org/lockwood/eia>`_